﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Escrow.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastPasswordChangeDate { get; set; }
        

        public int RecordStatus { get; set; }

        public string CreatedBy { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here => this.FirstName is a value stored in database against the user
            userIdentity.AddClaim(new Claim("FirstName", $"{FirstName} {LastName}"));
            //userIdentity.AddClaim(new Claim("AmountLimit", AmountLimit.ToString(CultureInfo.InvariantCulture)));

            // Add custom user claims here
            return userIdentity;
        }
    }
}