﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Escrow.ApiResponses;

namespace Escrow.Models
{
    public class EscrowModel
    {
        public WebFilter WebFilter { get; set; }

        public List<CustomersDto> Customers { get; set; }

        public UpdateModel UpdateModel { get; set; }

        public List<B2CRequest> B2CRequests { get; set; }

        public B2CModelFilter B2CModelFilter { get; set; }

        public List<TransactionRequest> TransactionRequests { get; set; }

        public TransactionRequest TransactionRequest { get; set; }

        public List<SelectListItem> DisputeResolutionMethods { get; set; }

        public DisputeResolutionMethod DisputeResolutionMethod { get; set; }
    }
}