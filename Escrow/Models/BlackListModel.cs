﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.Models
{
    public class BlackListModel
    {
        public Guid BlacklistHistoryId { get; set; }
        public Guid UserId { get; set; }
        public string UserStory { get; set; }
        public DateTime ListedAt { get; set; }
        public bool IsWhitelisting { get; set; }
        public string CreatedBy { get; set; }
    }
}