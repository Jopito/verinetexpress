﻿using Newtonsoft.Json;
using System;

namespace Escrow.Models
{
    public class CustomersDto
    {
        [JsonProperty("userId")]
        public Guid UserId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        [JsonProperty("countrId")]
        public Guid CountryId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("isBlacklisted")]
        public bool IsBlacklisted { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("fullyRegistered")]
        public bool FullyRegistered { get; set; }

        [JsonProperty("passwordSet")]
        public bool PasswordSet { get; set; }

        [JsonProperty("dateCreated")]
        public DateTime DateCreated { get; set; }

    }
}


