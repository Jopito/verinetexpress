﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.Models
{
    public class B2CModelFilter
    {
        public string DateFrom { get; set; }

        public string DateTo { get; set; }

        public string MsisdnOrEmail { get; set; }
    }
}