﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.Models
{
    public class UpdateModel
    {
        public string Msisdn { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Guid UserId { get; set; }

        public string BlackListReason { get; set; }
    }
}