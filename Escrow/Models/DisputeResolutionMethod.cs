﻿namespace Escrow.Models
{
    public class DisputeResolutionMethod
    {
        public int ResolutionMethod { get; set; }
        public string SettlementNotes { get; set; }
        public string TransactionId { get; set; }
        public int PayerAmount { get; set; }
        public int PayeeAmount { get; set; }
        public string UserName { get; set; }
        public string TransactionReference { get; set; }

    }
}