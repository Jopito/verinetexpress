﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Escrow.Models
{
    public class UserDetails
    {
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "UserName")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "The Last Name is required")]
        public string LastName { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "The Phone Number is required")]
        public string PhoneNumber { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastPasswordChangeDate { get; set; }

        [Display(Name = "2FA Enabled")]
        public bool TwoFactorEnabled { get; set; }

        [Display(Name = "Lock User ?")]
        public bool LockoutUser { get; set; }

        [Display(Name = "Roles")]

        public ICollection<string> AssignedRole { get; set; }
        
        public List<string> SystemRoles { get; set; }

        public string Actions { get; set; }

        [Display(Name = "Id")]
        public string Id { get; set; }

        [Display(Name = "Email Confirmed")]
        public bool EmailConfirmed { get; set; }

        public string Commands { get; set; }
    }
}