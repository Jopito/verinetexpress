﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Escrow.ApiResponses;

namespace Escrow.Models
{
    public class MasterTransactionQueryModel
    {
        public Status status { get; set; }

        public List<TransactionRequest> data { get; set; }

        public Paging paging { get; set; }
    }
}