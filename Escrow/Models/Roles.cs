﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Escrow.Models
{
    public class Roles
    {
        public Guid Id { get; set; }
        [Display(Name = "Role Name")]
        public string Name { get; set; }

       
        public string Commands { get; set; }

        public bool HasUsers { get; set; }
    }
}