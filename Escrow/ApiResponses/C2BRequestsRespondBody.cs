﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.ApiResponses
{
    public class C2BRequestsRespondBody
    {
        public Status status { get; set; }

        public List<MpesaPayment> data { get; set; }

        public Paging paging { get; set; }
    }
}