﻿using System.Collections.Generic;

namespace Escrow.ApiResponses
{
    public class TransactionsRequestsResponseBody
    {
        public Status status { get; set; }

        public List<TransactionRequest> data { get; set; }

        public Paging paging { get; set; }
    }
}