﻿namespace Escrow.ApiResponses
{
    public class Status
    {
        public string code { get; set; }

        public string message { get; set; }
    }
}