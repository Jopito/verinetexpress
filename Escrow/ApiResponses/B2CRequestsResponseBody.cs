﻿using System.Collections.Generic;

namespace Escrow.ApiResponses
{
    public class B2CRequestsResponseBody
    {
        public Status status { get; set; }

        public List<B2CRequest> data { get; set; }

        public Paging paging { get; set; }
    }
}