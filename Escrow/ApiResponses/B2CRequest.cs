﻿using System;

namespace Escrow.ApiResponses
{
    public class B2CRequest
    {
        public string Msisdn { get; set; }
        public decimal Amount { get; set; }
        public Guid MasterTransactionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ResponsePayload { get; set; }
        public byte Status { get; set; }
        public string StatusDescription { get; set; }
        public string MpesaCode { get; set; }
        public decimal B2CWorkingAccountAvailableFunds { get; set; }
        public decimal B2CUtilityAccountAvailableFunds { get; set; }
        public string ReceiverPartyPublicName { get; set; }
        public string MpesaConversationId { get; set; }
        public string OriginatorConversationId { get; set; }

        public string ResponseMessage { get; set; }

        public string MasterTransactionTransactionReference { get; set; }
        public string MasterTransactionPayeeToken { get; set; }

        public DateTime DisplayDate { get; set; }


        public string FormatedDate
        {

            get { return CreatedDate.ToString("dd/MM/yyyy hh:mm:ss"); }
        }


    }
}