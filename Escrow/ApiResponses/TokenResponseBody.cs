﻿using System.Collections.Generic;

namespace Escrow.ApiResponses
{
    public class TokenResponseBody
    {
        public Status status { get; set; }

        public List<TokenResponseWrapper> data { get; set; }

        public Paging paging { get; set; }
    }
}