﻿namespace Escrow.ApiResponses
{
    public class Paging
    {
        public string next { get; set; }

        public string previous { get; set; }

        public int totalCount { get; set; }
    }
}