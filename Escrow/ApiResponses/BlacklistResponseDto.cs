﻿using Escrow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.ApiResponses
{
    public class BlacklistResponseDto
    {
        public Status status { get; set; }

        public List<BlackListModel> data { get; set; }

        public Paging paging { get; set; }
    }
}