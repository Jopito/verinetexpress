﻿namespace Escrow.ApiResponses
{
    public class TokenResponseWrapper
    {
        public string accessToken { get; set; }

        public string expires { get; set; }

        public string tokenType { get; set; }
    }
}