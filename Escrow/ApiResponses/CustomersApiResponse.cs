﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Escrow.Models;

namespace Escrow.ApiResponses
{
    public class CustomersApiResponse
    {
        public Status status { get; set; }
    
        public List<CustomersDto> data { get; set; }

        public Paging paging { get; set; }
    }
}
