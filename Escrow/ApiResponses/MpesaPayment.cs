﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.ApiResponses
{
    public class MpesaPayment
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Msisdn { get; set; }

        public decimal Amount { get; set; }

        public string MpesaCode { get; set; }

        public string BusinessShortCode { get; set; }

        public string AccountReference { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal OrganizationBalance { get; set; }

        public string TransactionType { get; set; }

        public DateTime MpesaTransactionDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public byte Status { get; set; }
    }
}