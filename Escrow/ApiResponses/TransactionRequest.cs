﻿using System;

namespace Escrow.ApiResponses
{
    public class TransactionRequest
    {
        public int PaymentMethod { get; set; }
        public string PaymentMethodDescription { get; set; }
        public long NetAmount { get; set; }
        public string Narrative { get; set; }
        public Guid PayerId { get; set; }
        public string PayeeId { get; set; }
        public string PayeeToken { get; set; }
        public string PayerToken { get; set; }
        public Guid MasterTransactionId { get; set; }
        public byte Status { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeDescription { get; set; }
        public string StatusDescription { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public decimal TotalPayable { get; set; }
        public string TransactionReference { get; set; }


        public DateTime LocalDate { get; set; }

        public DateTime DisplayDate { get; set; }
        

        public string Commands { get; set; }

    }
}