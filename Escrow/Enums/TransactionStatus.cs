﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.Enums
{
    public enum TransactionStatus
    {
        Processing,
        Failed,
        Pending,
        Complete,
        Cancelled,
        Disputed,
        Reversed,
        Refunded,
        Resolved
    }

}