﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escrow.Enums
{
    public enum DisputeResolutionMethods
    {
        ByApproveToRecepient = 1,
        ByReverseToSender,
        BySplit
    }
}