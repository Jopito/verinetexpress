﻿using Newtonsoft.Json;
using System;

namespace Escrow.ApiRequests
{
    public class TransactionsRangeFilterRequest
    {
        [JsonProperty("date_from")]
        public DateTime DateFrom { get; set; }

        [JsonProperty("date_to")]
        public DateTime DateTo { get; set; }
    }
}