﻿using Newtonsoft.Json;

namespace Escrow.ApiRequests
{
    public class TokenRequest
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }
        [JsonProperty("appSecret")]
        public string AppSecret { get; set; }
    }
}