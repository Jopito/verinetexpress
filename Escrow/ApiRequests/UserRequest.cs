﻿using Newtonsoft.Json;

namespace Escrow.ApiRequests
{
    public class UserRequest
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        [JsonProperty("countryId")]
        public string CountryId { get; set; }
    }
}