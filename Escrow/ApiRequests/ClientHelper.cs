﻿using System;
using System.Configuration;
using System.Linq;
using Escrow.ApiResponses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using System.IO;

namespace Escrow.ApiRequests
{
    public class ClientHelper
    {
       
       public static string BearerToken { get; set; }

       public static Uri _baseAddress = new Uri("https://caveatapi.azurewebsites.net/");


        public static async Task<string> GenerateToken()
        {
            HttpClient client = new HttpClient();

            TokenResponseBody responseBody = new TokenResponseBody();

            client.BaseAddress = _baseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Clear();
            JsonResult result = new JsonResult();
            try
            {
                TokenRequest token = new TokenRequest
                {
                    ApiKey = "4e1d0a10-67ab-4701-8350-2f3982de0e2d",
                    AppSecret = "8709c78870874315a4d11f4e22730b21-efced12e-c284-4cb0-959e-42a53a0c7865",
                    UserId = "ea7a8be1-cf54-41ed-afbe-64dd74e33114"
                };

                string json = JsonConvert.SerializeObject(token);

                HttpContent _Body = new StringContent(json);

                _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync(
               "/Auth/CreateToken", _Body);


                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                responseBody = value.ToObject<TokenResponseBody>();

                BearerToken = responseBody.data.FirstOrDefault().accessToken;

                return BearerToken;

            }
            catch (Exception e)
            {
                return null;
            }

        }

        public static HttpClient GetClient(string BearerToken)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = _baseAddress;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", BearerToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Accept.Clear();

            return client;
        }

        public async static Task<string> ProcessGetRequest(string url)
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);

            string accessToken = await GenerateToken();

            httpRequest.Headers.Add("Authorization", "Bearer " + accessToken);

            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();


            string result = string.Empty;
            using (var responseStream = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = await responseStream.ReadToEndAsync();

                return result;
            }
        }
    }
}