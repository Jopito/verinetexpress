﻿using Escrow.ApiRequests;
using Escrow.ApiResponses;
using Escrow.Helpers;
using Escrow.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Escrow.Controllers
{
    [Authorize]
    public class C2BRequestsController : BaseController
    {
        // GET: C2BRequests
        public ActionResult Index()
        {
            return View();
        }

        [NonAction]
        public async Task<List<MpesaPayment>> C2BTransactionList()
        {

            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            C2BRequestsRespondBody C2BRequestsResponseBody = new C2BRequestsRespondBody();

            List<MpesaPayment> result = new List<MpesaPayment>();
            try
            {
                TransactionsRangeFilterRequest tranrequest = new TransactionsRangeFilterRequest();
                tranrequest.DateFrom=DateTime.UtcNow.AddMonths(-3);
                tranrequest.DateTo=DateTime.Today;

                string json = JsonConvert.SerializeObject(tranrequest);

                HttpContent body = new StringContent(json);

                body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await Client.PostAsync(
               "api/Transaction/GetMpesaPaymentsByDate", body);

                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                C2BRequestsResponseBody = value.ToObject<C2BRequestsRespondBody>();

                result = C2BRequestsResponseBody.data;

                return result;


            }
            catch (Exception e)
            {
                return new List<MpesaPayment>();

            }
           
        }

        [HttpPost]
        public async Task<JsonResult> TransactionRequestsList(JQueryDataTablesModel datatablemodel)
        {

            var pagecollection = await C2BTransactionList();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var transactionList = GetAllTransactions(startIndex: datatablemodel.iDisplayStart,
               pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
               totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, TransactionList: pagecollection);

            return Json(new JQueryDataTablesResponse<MpesaPayment>(items: transactionList,
               totalRecords: totalRecordCount,
               totalDisplayRecords: searchRecordCount,
               sEcho: datatablemodel.sEcho));
        }

        public static IList<MpesaPayment> GetAllTransactions(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<MpesaPayment> TransactionList)
        {
            var transactions = TransactionList;
            totalRecordCount = transactions.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                transactions = transactions.Where(c => c.AccountReference
                    .ToLower().Contains(searchString.ToLower()) || c.BusinessShortCode.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = transactions.Count;

            IOrderedEnumerable<MpesaPayment> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "FirstName":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.FirstName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.FirstName);
                        break;

                    case "LastName":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.LastName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.LastName);
                        break;
                    case "Msisdn":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.Msisdn)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Msisdn);
                        break;

                    case "Amount":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.Amount)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Amount);
                        break;

                    case "MpesaCode":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.MpesaCode)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.MpesaCode);
                        break;

                    case "BusinessShortCode":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.BusinessShortCode)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.BusinessShortCode);
                        break;

                    case "InvoiceNumber":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.InvoiceNumber)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.InvoiceNumber);
                        break;
                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }
    }
}