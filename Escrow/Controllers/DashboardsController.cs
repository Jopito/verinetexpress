﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Escrow.Controllers
{
    [Authorize]
    public class DashboardsController : Controller
    {
        public ActionResult Dashboard()
        {
            return View();
        }

    }
}