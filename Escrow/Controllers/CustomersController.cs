﻿using Escrow.ApiRequests;
using Escrow.ApiResponses;
using Escrow.Helpers;
using Escrow.Infrastructure;
using Escrow.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Escrow.Controllers
{


    [Authorize]
    public class CustomersController : BaseController
    {

        string baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        EscrowModel escrowModel = new EscrowModel();
        // GET: Customers

        public ActionResult Index()
        {
            escrowModel.WebFilter = new WebFilter();
            escrowModel.Customers = new List<CustomersDto>();
            return View(escrowModel);
        }

        [HttpPost]
        public async Task<ActionResult> Search([Bind]WebFilter webFilter)
        {

            List<CustomersDto> customers = new List<CustomersDto>();
            if (string.IsNullOrEmpty(webFilter.DateFrom) && string.IsNullOrEmpty(webFilter.MsisdnOrEmail))
            {
                TempData["Error"] = "Invalid dates selected";

                return RedirectToAction("Index");
            }


            if (!string.IsNullOrEmpty(webFilter.MsisdnOrEmail))
            {
                List<CustomersDto> result = new List<CustomersDto>();
                CustomersApiResponse customerData = new CustomersApiResponse();

                string url = string.Format("{0}{1}{2}", baseUrl, "api/User/GetUserByMsisdnOrEmail/", webFilter.MsisdnOrEmail);

                var apiResult = await ClientHelper.ProcessGetRequest(url);

                JObject value = JObject.Parse(apiResult);

                customerData = value.ToObject<CustomersApiResponse>();

                customers = customerData.data;


            }
            else
            {
                DateTime dateFrom = DateTime.ParseExact(webFilter.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(webFilter.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                customers = await GetUsers(dateFrom, dateTo);
            }


            if (customers == null || customers.Count <= 0)
            {
                customers = new List<CustomersDto>();

                TempData["Error"] = "Unable to locate customers from the specified dates";

                escrowModel.Customers = customers;

                return View("Index", escrowModel);

            }


            customers = customers.OrderByDescending(a => a.DateCreated).ToList();


            escrowModel.Customers = customers;

            return View("Index", escrowModel);
        }


        public async Task<ActionResult> Edit(Guid id)
        {
            List<CustomersDto> result = new List<CustomersDto>();
            CustomersApiResponse customerData = new CustomersApiResponse();

            string url = string.Format("{0}{1}{2}", baseUrl, "/api/User/GetUser/", id);

            var apiResult = await ClientHelper.ProcessGetRequest(url);

            JObject value = JObject.Parse(apiResult);

            customerData = value.ToObject<CustomersApiResponse>();


            CustomersDto customer = customerData.data.FirstOrDefault();

            escrowModel.UpdateModel = new UpdateModel
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Msisdn = customer.Msisdn,
                UserId = customer.UserId
            };

            return View("Edit", escrowModel);
        }

        [NonAction]
        public async Task<List<CustomersDto>> GetUsers(DateTime dateFrom, DateTime dateTo)
        {


            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            CustomersApiResponse customersApiResponse = new CustomersApiResponse();

            List<CustomersDto> result = new List<CustomersDto>();
            try
            {
                TransactionsRangeFilterRequest tranrequest = new TransactionsRangeFilterRequest();

                tranrequest.DateFrom = dateFrom;

                tranrequest.DateTo = dateTo;

                string json = JsonConvert.SerializeObject(tranrequest);

                HttpContent _Body = new StringContent(json);

                _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await Client.PostAsync(
                    "api/User/GetPlatformUsers", _Body);

                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                customersApiResponse = value.ToObject<CustomersApiResponse>();

                customersApiResponse.data = customersApiResponse.data.OrderByDescending(a => a.DateCreated).ToList();

                result = customersApiResponse.data;
               


                return result;


            }
            catch (Exception e)
            {
                return new List<CustomersDto>();

            }

        }

        //[HttpPost]
        //public async Task<JsonResult> CustomersRequestsList(JQueryDataTablesModel datatablemodel)
        //{

        //    var pagecollection = await GetUsers();

        //    int totalRecordCount = 0;

        //    int searchRecordCount = 0;

        //    var customers = GetAllUsers(startIndex: datatablemodel.iDisplayStart,
        //        pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
        //        totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, customers: pagecollection);

        //    return Json(new JQueryDataTablesResponse<CustomersDto>(items: customers,
        //        totalRecords: totalRecordCount,
        //        totalDisplayRecords: searchRecordCount,
        //        sEcho: datatablemodel.sEcho));
        //}

        [NonAction]
        public static IList<CustomersDto> GetAllUsers(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomersDto> customers)
        {
            var data = customers;
            totalRecordCount = data.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                data = customers.Where(c => c.FirstName
                    .ToLower().Contains(searchString.ToLower()) ||
                    c.LastName.ToLower().Contains(searchString.ToLower()) ||
                    c.IsActive.ToString().ToLower().Contains(searchString.ToLower()) ||
                    c.Msisdn.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = data.Count;

            IOrderedEnumerable<CustomersDto> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "FirstName":
                        sortedTrans = sortedTrans == null ? data.CustomSort(sortedColumn.Direction, tran => tran.FirstName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.FirstName);
                        break;

                    case "LastName":
                        sortedTrans = sortedTrans == null ? data.CustomSort(sortedColumn.Direction, tran => tran.LastName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.LastName);
                        break;
                    case "Msisdn":
                        sortedTrans = sortedTrans == null ? data.CustomSort(sortedColumn.Direction, tran => tran.Msisdn)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Msisdn);
                        break;
                    case "FormatedDate":
                        sortedTrans = sortedTrans == null ? data.CustomSort(sortedColumn.Direction, tran => tran.DateCreated)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.DateCreated);
                        break;

                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }

        public async Task<ActionResult> BlackListUser([Bind]UpdateModel updateModel)
        {
            if (!ModelState.IsValid)
            {

                TempData["Error"] = string.Join(" |", ModelState.Select(a => a.Value.Errors.Select(b => b.ErrorMessage)));

                return RedirectToAction("Index");
            }

            var Client = ClientHelper.GetClient(_bearer);

            dynamic postData = new ExpandoObject();
            postData.black_list_by = User.Identity.Name;
            postData.user_id = updateModel.UserId;
            postData.reason = updateModel.BlackListReason;

            string data = JsonConvert.SerializeObject(postData);

            HttpContent _Body = new StringContent(data);

            _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await Client.PostAsync(
                "/api/User/DisableClientService", _Body);

            JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            BlacklistResponseDto customerData = value.ToObject<BlacklistResponseDto>();

            if (customerData.status?.code == Convert.ToInt32(HttpStatusCode.InternalServerError).ToString())
            {
                TempData["Error"] = customerData.status.message;
            }
            else
            {
                TempData["Success"] = customerData.status.message;

            }


            return RedirectToAction("Index");



        }

    }
}