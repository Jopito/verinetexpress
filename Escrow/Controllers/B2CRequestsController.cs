﻿using Escrow.ApiRequests;
using Escrow.ApiResponses;
using Escrow.Helpers;
using Escrow.Infrastructure;
using Escrow.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Escrow.Controllers
{
    [Authorize]
    public class B2CRequestsController : BaseController
    {
        EscrowModel escrowModel = new EscrowModel();

        // GET: B2CRequests
        public async Task<ActionResult> Index()
        {

            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;


            List<B2CRequest> b2CRequests = await B2CTransactionList(dateFrom, dateTo);

            escrowModel.B2CRequests = b2CRequests;
            escrowModel.B2CModelFilter = new B2CModelFilter();

            return View(escrowModel);
        }





        public async Task<ActionResult> FilterB2CRequests([Bind]B2CModelFilter b2CModelFilter)
        {

            if (string.IsNullOrEmpty(b2CModelFilter.DateFrom) || string.IsNullOrEmpty(b2CModelFilter.DateTo))
            {


                TempData["Error"] = "Invalid  dates selected";

                return RedirectToAction("Index");
            }


            DateTime dateFrom = DateTime.ParseExact(b2CModelFilter.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dateTo = DateTime.ParseExact(b2CModelFilter.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            List<B2CRequest> b2CRequests = await B2CTransactionList(dateFrom, dateTo);
            if (b2CRequests.Count > 0)
            {
                b2CRequests = b2CRequests.OrderByDescending(a => a.DisplayDate).ToList();
            }

            escrowModel.B2CRequests = b2CRequests;



            return View("Index", escrowModel);

        }

        public async Task<List<B2CRequest>> B2CTransactionList(DateTime dateFrom, DateTime dateTo)
        {

            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            B2CRequestsResponseBody B2CRequestsResponseBody = new B2CRequestsResponseBody();

            List<B2CRequest> result = new List<B2CRequest>();
            try
            {
                TransactionsRangeFilterRequest tranrequest = new TransactionsRangeFilterRequest();
                tranrequest.DateFrom = dateFrom;
                tranrequest.DateTo = dateTo;

                string json = JsonConvert.SerializeObject(tranrequest);

                HttpContent body = new StringContent(json);

                body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await Client.PostAsync(
               "api/Transaction/GetMpesaB2CRequests", body);

                string result1 = response.Content.ReadAsStringAsync().Result;
                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                B2CRequestsResponseBody = value.ToObject<B2CRequestsResponseBody>();

                result = B2CRequestsResponseBody.data;

                return result;



            }
            catch (Exception e)
            {
                return new List<B2CRequest>();

            }

        }

        //[HttpPost]
        //public async Task<JsonResult> TransactionRequestsList(JQueryDataTablesModel datatablemodel)
        //{

        //    var pagecollection = await B2CTransactionList();

        //    int totalRecordCount = 0;

        //    int searchRecordCount = 0;

        //    var transactionList = GetAllB2CTransactions(startIndex: datatablemodel.iDisplayStart,
        //       pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
        //       totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, TransactionList: pagecollection);

        //    return Json(new JQueryDataTablesResponse<B2CRequest>(items: transactionList,
        //       totalRecords: totalRecordCount,
        //       totalDisplayRecords: searchRecordCount,
        //       sEcho: datatablemodel.sEcho));
        //}

        [NonAction]
        public static IList<B2CRequest> GetAllB2CTransactions(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<B2CRequest> TransactionList)
        {
            var transactions = TransactionList;
            totalRecordCount = transactions.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                transactions = transactions.Where(c => c.Msisdn
                    .ToLower().Contains(searchString.ToLower()) || c.MpesaCode.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = transactions.Count;

            IOrderedEnumerable<B2CRequest> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "Msisdn":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.Msisdn)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Msisdn);
                        break;

                    case "Amount":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.Amount)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Amount);
                        break;
                    case "MasterTransactionId":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.MasterTransactionId)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.MasterTransactionId);
                        break;

                    case "MpesaCode":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.MpesaCode)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.MpesaCode);
                        break;

                    case "CreateDate":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.CreatedDate)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.CreatedDate);
                        break;
                    case "ReceiverPartyPublicName":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.ReceiverPartyPublicName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.CreatedDate);
                        break;

                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }
    }
}