﻿using Escrow.ApiRequests;
using Escrow.ApiResponses;
using Escrow.Enums;
using Escrow.Helpers;
using Escrow.Infrastructure;
using Escrow.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Escrow.Controllers
{
    public class TransactionRequestsController : BaseController
    {
        // GET: TransactionRequests

        const int rate = 100;

        private string baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        EscrowModel escrowModel = new EscrowModel();
        public async Task<ActionResult> Index()
        {

            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;

            List<TransactionRequest> transactions = await TransactionRequestsList(dateFrom, dateTo);

            transactions.ForEach(a => a.LocalDate = UtilityManager.Utility.FormalizeTranscationDate(a.TransactionDate, a.TransactionTime));

            transactions.ForEach(a => a.NetAmount = a.NetAmount / rate);


            escrowModel.TransactionRequests = transactions;

            transactions = transactions.OrderByDescending(a => a.DisplayDate).ToList();

            return View(escrowModel);
        }

        [HttpGet]
        public async Task<ActionResult> SettleDisputedTransaction(Guid id)
        {
            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            TransactionRequest transactionRequest = new TransactionRequest();

            TransactionsRequestsResponseBody TransactionsRequestsResponseBody = new TransactionsRequestsResponseBody();


            try
            {
                HttpResponseMessage response = await Client.GetAsync(
              "/api/Transaction/GetTransactionById/" + id);

                ViewBag.Disputes = DisputeCategorySelectListItems(string.Empty);

                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                TransactionsRequestsResponseBody = value.ToObject<TransactionsRequestsResponseBody>();

                transactionRequest = TransactionsRequestsResponseBody.data.FirstOrDefault();

            }
            catch (Exception ex)
            {
                return RedirectToAction("index");
            }

            return View(transactionRequest);
        }

        public async Task<ActionResult> SettleDisputedTransaction(TransactionRequest transactionRequest)
        {
            return View();
        }

        [NonAction]
        public async Task<List<TransactionRequest>> TransactionRequestsList(DateTime dateFrom, DateTime dateTo)
        {


            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            TransactionsRequestsResponseBody TransactionsRequestsResponseBody = new TransactionsRequestsResponseBody();

            List<TransactionRequest> result = new List<TransactionRequest>();
            try
            {
                TransactionsRangeFilterRequest tranrequest = new TransactionsRangeFilterRequest();

                tranrequest.DateFrom = dateFrom;

                tranrequest.DateTo = dateTo;

                string json = JsonConvert.SerializeObject(tranrequest);

                HttpContent _Body = new StringContent(json);

                _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await Client.PostAsync(
               "api/Transaction/GetTransactionsByDate", _Body);

                JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                TransactionsRequestsResponseBody = value.ToObject<TransactionsRequestsResponseBody>();

                result = TransactionsRequestsResponseBody.data;

                return result;


            }
            catch (Exception e)
            {
                return new List<TransactionRequest>();

            }

        }

        [HttpPost]
        public async Task<ActionResult> FilterTransactions([Bind]B2CModelFilter b2CModelFilter)
        {

            if (string.IsNullOrEmpty(b2CModelFilter.DateFrom) && string.IsNullOrEmpty(b2CModelFilter.MsisdnOrEmail))
            {
                TempData["Error"] = "Invalid dates selected";

                return RedirectToAction("Index");
            }

            List<TransactionRequest> transactionRequests = new List<TransactionRequest>();



            if (!string.IsNullOrEmpty(b2CModelFilter.MsisdnOrEmail))
            {
                transactionRequests = await TransactionByReferenceLookup(b2CModelFilter.MsisdnOrEmail);
            }
            else
            {
                DateTime dateFrom = DateTime.ParseExact(b2CModelFilter.DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dateTo = DateTime.ParseExact(b2CModelFilter.DateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                transactionRequests = await TransactionRequestsList(dateFrom, dateTo);

            }


            if (transactionRequests == null || transactionRequests.Count <= 0)
            {
                TempData["Error"] = "Unable to locate transactions from the selected dates";

                return RedirectToAction("Index");
            }

            try
            {
                transactionRequests.ForEach(a => a.NetAmount = a.NetAmount / rate);
            }
            catch (Exception)
            {

                transactionRequests = new List<TransactionRequest>();
                TempData["Error"] = "Unable to locate transaction(s)";

            }

            if (transactionRequests.Count > 0)
            {
                transactionRequests = transactionRequests.OrderByDescending(a => a.DisplayDate).ToList();
            }

            escrowModel.TransactionRequests = transactionRequests;
            return View("Index", escrowModel);

        }

        [HttpGet]
        public async Task<ActionResult> GetTransactionDetailsByReference(string reference)
        {

            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);
            TransactionRequest queryModel = new TransactionRequest();
            string fullUrl = string.Format("{0}{1}", baseUrl, "");

            dynamic postData = new ExpandoObject();
            postData.transactionReference = reference;


            string json = JsonConvert.SerializeObject(postData);

            HttpContent _Body = new StringContent(json);

            _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await Client.PostAsync(
                "api/Transaction/GetTransactionByReference", _Body);

            JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            MasterTransactionQueryModel masterTransactionQueryModel = value.ToObject<MasterTransactionQueryModel>();

            List<TransactionRequest> result = masterTransactionQueryModel.data;


            result.Add(queryModel);

            result.ForEach(a => a.NetAmount = a.NetAmount / rate);


            escrowModel.TransactionRequests = result;

            return View("Index", escrowModel);


        }

        public async Task<ActionResult> DisputedTransaction(string reference)
        {

            List<TransactionRequest> transactionRequests = await TransactionByReferenceLookup(reference);



            transactionRequests.ForEach(a => a.NetAmount = a.NetAmount / rate);

            TransactionRequest transactionRequest = transactionRequests.FirstOrDefault();


            escrowModel.DisputeResolutionMethods = GetEnumListItemsAsync<DisputeResolutionMethods>();

            escrowModel.TransactionRequest = transactionRequest;

            escrowModel.DisputeResolutionMethod = new DisputeResolutionMethod();

            return View("DisputedTransaction", escrowModel);
        }
        public List<SelectListItem> GetEnumListItemsAsync<T>()
        {
            List<T> values = Enum.GetValues(typeof(T)).Cast<T>().ToList();

            int totalItems = values.Count;
            List<SelectListItem> selectList = new List<SelectListItem>();
            for (int i = 0; i < totalItems; i++)
            {
                selectList.Add(new SelectListItem
                {
                    Value = Convert.ToInt32(values[i]).ToString(),
                    Text = values[i].ToString(),
                });
            }

            return selectList;
        }


        public async Task<ActionResult> ResolveTransaction([Bind]DisputeResolutionMethod disputeResolutionMethod)
        {


            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join("|", ModelState.Select(a => a.Value).SelectMany(a => a.Errors.Select(b => b.ErrorMessage)));

                return RedirectToAction("Index");
            }

            string transactionId = Request["transactionId"];
            string transactionReference = Request["reference"];
            disputeResolutionMethod.TransactionId = transactionId;
            disputeResolutionMethod.TransactionReference = transactionReference;

            if (string.IsNullOrEmpty(disputeResolutionMethod.SettlementNotes))
            {

                TempData["Error"] = "Please enter settlement notes";

                return RedirectToAction("DisputedTransaction", new { reference = disputeResolutionMethod.TransactionReference });
            }

            string apiEndpoint = string.Empty;



            disputeResolutionMethod.TransactionReference = transactionReference;
            switch (disputeResolutionMethod.ResolutionMethod)
            {
                case (byte)DisputeResolutionMethods.BySplit:



                    if (disputeResolutionMethod.PayeeAmount <= 0 || disputeResolutionMethod.PayerAmount <= 0)
                    {
                        TempData["Error"] = "Please enter both sender and recepient amount";

                        return RedirectToAction("DisputedTransaction", new { reference = disputeResolutionMethod.TransactionReference });

                    }

                    apiEndpoint = "/api/Transaction/SettleDisputedTransactionBySplit";
                    break;
                case (byte)DisputeResolutionMethods.ByApproveToRecepient:
                    apiEndpoint = "/api/Transaction/SettleDisputedTransactionByApprove";
                    break;
                case (byte)DisputeResolutionMethods.ByReverseToSender:
                    apiEndpoint = "/api/Transaction/SettleDisputedTransactionByReverse";
                    break;
            }


            dynamic postData = new ExpandoObject();
            postData.settlementNotes = disputeResolutionMethod.SettlementNotes;
            postData.transactionId = disputeResolutionMethod.TransactionId;
            postData.username = User.Identity.Name;
            postData.payerAmount = disputeResolutionMethod.PayerAmount * rate;
            postData.payeeAmount = disputeResolutionMethod.PayeeAmount * rate;
            postData.transactionReference = transactionReference;

            string json = JsonConvert.SerializeObject(postData);

            JObject response = await PostData(apiEndpoint, json);

            ReversalResponse reversalResponse = response.ToObject<ReversalResponse>();
            if (reversalResponse.status.code == Convert.ToInt32(HttpStatusCode.OK).ToString())
            {
                TempData["Success"] = reversalResponse.status.message;
            }

            else
            {
                TempData["Error"] = reversalResponse.status.message;

            }


            return RedirectToAction("Index");
        }



        private async Task<JObject> PostData(string apiEndpoint, string postData)
        {
            var Client = ClientHelper.GetClient(_bearer);

            string fullUrl = string.Format("{0}{1}", baseUrl, apiEndpoint);

            HttpContent _Body = new StringContent(postData);

            _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await Client.PostAsync(
                fullUrl, _Body);

            JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);


            return value;
        }



        private async Task<List<TransactionRequest>> TransactionByReferenceLookup(string reference)
        {

            await this.CheckIfBearerTokenIsSet();

            var Client = ClientHelper.GetClient(_bearer);

            string fullUrl = string.Format("{0}{1}", baseUrl, "");

            dynamic postData = new ExpandoObject();
            postData.transactionReference = reference;


            string json = JsonConvert.SerializeObject(postData);

            HttpContent _Body = new StringContent(json);

            _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await Client.PostAsync(
                "api/Transaction/GetTransactionByReference", _Body);

            JObject value = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            MasterTransactionQueryModel masterTransactionQueryModel = value.ToObject<MasterTransactionQueryModel>();

            List<TransactionRequest> result = masterTransactionQueryModel.data;

            return result;

        }
        //[HttpPost]
        //public async Task<JsonResult> TransactionRequestsList(JQueryDataTablesModel datatablemodel)
        //{

        //    var pagecollection = await TransactionRequestsList();

        //    int totalRecordCount = 0;

        //    int searchRecordCount = 0;

        //    var transactionList = GetAllTransactions(startIndex: datatablemodel.iDisplayStart,
        //       pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
        //       totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, TransactionList: pagecollection);

        //    return Json(new JQueryDataTablesResponse<TransactionRequest>(items: transactionList,
        //       totalRecords: totalRecordCount,
        //       totalDisplayRecords: searchRecordCount,
        //       sEcho: datatablemodel.sEcho));
        //}
        [NonAction]
        public static IList<TransactionRequest> GetAllTransactions(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<TransactionRequest> TransactionList)
        {
            var transactions = TransactionList;
            totalRecordCount = transactions.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                transactions = transactions.Where(c => c.PayeeToken
                    .ToLower().Contains(searchString.ToLower()) ||
                    c.TransactionReference.ToLower().Contains(searchString.ToLower()) ||
                    c.Status.ToString().ToLower().Contains(searchString.ToLower()) ||
                    c.PaymentMethodDescription.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = transactions.Count;

            IOrderedEnumerable<TransactionRequest> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "TransactionDate":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.TransactionDate)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.TransactionDate);
                        break;

                    case "PaymentMethodDescription":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.PaymentMethodDescription)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.PaymentMethodDescription);
                        break;
                    case "NetAmount":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.NetAmount)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.NetAmount);
                        break;

                    case "PayeeToken":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.PayeeToken)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.PayeeToken);
                        break;

                    case "TransactionReference":
                        sortedTrans = sortedTrans == null ? transactions.CustomSort(sortedColumn.Direction, tran => tran.TransactionReference)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.TransactionReference);
                        break;
                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }
    }
}