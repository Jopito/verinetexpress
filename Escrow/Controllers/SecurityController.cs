﻿using Escrow.Helpers;
using Escrow.Infrastructure;
using Escrow.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Escrow.Identity;
using ApplicationUser = Escrow.Models.ApplicationUser;

namespace Escrow.Controllers
{
    [Authorize]
    public class SecurityController : Controller
    {

        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly ApplicationUserManager _applicationUserManager;

        public SecurityController()
        {
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());

            _applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new Escrow.Models.ApplicationDbContext()));


        }

        public SecurityController(ApplicationUserManager userManager)
        {
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());

            _applicationUserManager = userManager;

        }


        #region Roles
        // GET: Security
        public async Task<ActionResult> Index()
        {

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Roles(JQueryDataTablesModel datatablemodel)
        {

            var pagecollection = _roleManager.Roles.ToList();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var rolesList = GetAllRoles(startIndex: datatablemodel.iDisplayStart,
              pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
              totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, RoleList: pagecollection);

            return Json(new JQueryDataTablesResponse<IdentityRole>(items: rolesList,
              totalRecords: totalRecordCount,
              totalDisplayRecords: searchRecordCount,
              sEcho: datatablemodel.sEcho));
        }

        [NonAction]
        public static IList<IdentityRole> GetAllRoles(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<IdentityRole> RoleList)
        {
            var roles = RoleList;
            totalRecordCount = roles.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                roles = roles.Where(c => c.Name
                    .ToLower().Contains(searchString.ToLower()) || c.Id.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = roles.Count;

            IOrderedEnumerable<IdentityRole> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "Name":
                        sortedTrans = sortedTrans == null ? roles.CustomSort(sortedColumn.Direction, tran => tran.Name)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.Name);
                        break;


                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public async Task<ActionResult> CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateRole(Roles role)
        {
            if (string.IsNullOrEmpty(role.Name))
            {
                TempData["ModelError"] = "Role Name is required";

                return View("CreateRole");
            }


            var roleCreate = new IdentityRole(role.Name);

            if (_roleManager.RoleExists(roleCreate.Name))
            {
                TempData["ModelError"] = "Role Exists Already";

                return View();
            }

            var identityResult = _roleManager.Create(roleCreate);

            if (identityResult != null)
            {
                TempData["Success"] = "Role Created Successfully";

                return RedirectToAction("Index");
            }

            return View();
        }


        [HttpGet]
        public async Task<ActionResult> EditRole(string id)
        {
            var identityResult = _roleManager.FindById(id);

            if (identityResult == null)
            {
                TempData["Error"] = "Role not found";

                return RedirectToAction("Roles");
            }

            var roles = new Roles
            {
                Id = Guid.Parse(id),
                Name = identityResult.Name
            };
            return View("editrole", roles);

        }

        [HttpPost]
        public async Task<ActionResult> EditRole(Roles role)
        {
            if (string.IsNullOrEmpty(role.Name))
            {
                TempData["Error"] = "Role name is required.";

                return RedirectToAction("Roles");
            }

            var roleUpdate = new IdentityRole
            {
                Id = role.Id.ToString(),
                Name = role.Name
            };

            var identityRole = _roleManager.FindById(roleUpdate.Id);

            if (identityRole != null)
            {
                if (identityRole.Name == role.Name)
                {
                    TempData["Success"] = "Request completed successfully.";

                    return RedirectToAction("Roles");
                }
            }
            else
            {
                TempData["Error"] = "Unable to find a role with the specified id";

                return RedirectToAction("Roles");
            }

            identityRole.Name = role.Name;

            var updateResult = _roleManager.Update(identityRole);


            var roles = new Roles { Name = role.Name, Id = role.Id };
            if (updateResult == null)
            {
                TempData["Error"] = "Role could not be updated.Please try again";

                return View("editrole", roles);
            }
            TempData["Success"] = "Role Updated Successfully";

            return RedirectToAction("Roles");

        }



        #endregion


        #region users
        [HttpGet]
        public async Task<ActionResult> CreateUser()
        {
            ViewBag.RolesList = GetUserRoleInfo(string.Empty, string.Empty);

            var userRegistration = new UserDetails { PhoneNumber = "254" };


            return View(userRegistration);
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser(UserDetails userDetails)
        {
            var applicationUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            ViewBag.RolesList = GetUserRoleInfo(string.Empty, string.Empty);


            var userRegistration = new UserDetails { PhoneNumber = "254" };

            if (!ModelState.IsValid)
            {
                var errorMessages = string.Join(" |",
                    ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                ModelState.AddModelError("", errorMessages);

                ViewBag.ModelError = "Invalid Model State";

                return View(userDetails);
            }

            var userCheck = await _applicationUserManager.FindByEmailAsync(userDetails.Email);

            if (userCheck != null)
            {
                TempData["Error"] = "User with specified email exists already";

                return View(userDetails);
            }

            var userNameCheck = _applicationUserManager.FindByName(userDetails.UserName);

            if (userNameCheck != null)
            {
                TempData["Error"] = "User with specified username exists already";

                return View("createuser", userDetails);
            }

            var userRoles = string.Join(",", userDetails.AssignedRole);

            string status = string.Empty;

            if (_applicationUserManager.FindByName(userDetails.UserName) == null)
            {

                string newPassword = PasswordStore.GenerateRandomPassword(new PasswordOptions
                {
                    RequiredLength = 10,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                    RequireNonAlphanumeric = true,
                    RequiredUniqueChars = 1
                });

                var user = new ApplicationUser
                {
                    UserName = userDetails.UserName,
                    Email = userDetails.Email,
                    EmailConfirmed = true,
                    TwoFactorEnabled = userDetails.TwoFactorEnabled,
                    PhoneNumber = userDetails.PhoneNumber,
                    PhoneNumberConfirmed = true,
                    LockoutEnabled = true,
                    FirstName = userDetails.FirstName,
                    LastName = userDetails.LastName,
                    LastPasswordChangeDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    RecordStatus = (int)RecordStatus.Approved

                };


                var identityResult = _applicationUserManager.Create(user, newPassword);

                var result = new IdentityResult();

                if (identityResult.Succeeded)
                {
                    var findUser = _applicationUserManager.FindByName(userDetails.UserName);

                    if (!_applicationUserManager.IsInRole(findUser.Id, userRoles))
                    {
                        string[] roles = userRoles.Split(',');

                        foreach (var items in roles)
                        {
                            result = _applicationUserManager.AddToRole(findUser.Id, items);
                        }

                        if (!result.Succeeded)
                        {
                            _applicationUserManager.Delete(user);

                            status = identityResult.Errors.FirstOrDefault();
                        }
                    }

                    await _applicationUserManager.SendEmailAsync(user.Id, " Verinet Login Credentials ", "Username: " + user.UserName + " Password: " + newPassword);
                }


            }

            if (!status.Equals("Success"))
            {
                TempData["Error"] = "User could not be created. Please try again";

                return RedirectToAction("Users");
            }
            TempData["Success"] = "User Registered Successfully";

            return RedirectToAction("Users");

        }

        [HttpGet]
        public async Task<ActionResult> EditUser(string id)
        {
            var applicationUser = await _applicationUserManager.FindByIdAsync(id);

            if (applicationUser != null)
            {
                var userRole = _applicationUserManager.GetRoles(id).FirstOrDefault();

                var roleId = userRole != null ? _roleManager.FindByName(userRole) : null;

                var userDetails = new UserDetails
                {
                    UserName = applicationUser.UserName,
                    LockoutUser = applicationUser.LockoutEndDateUtc == null ? false : true,
                    Email = applicationUser.Email,
                    PhoneNumber = applicationUser.PhoneNumber,
                    FirstName = applicationUser.FirstName,
                    LastName = applicationUser.LastName,
                    TwoFactorEnabled = applicationUser.TwoFactorEnabled,
                    Id = applicationUser.Id,

                };

                ViewBag.RolesList = GetUserRoleInfo(string.Empty, roleId != null ? roleId.Id : string.Empty);


                return View(userDetails);

            }
            else
            {
                TempData["Error"] = "Unable to find user with the specified id";

                return RedirectToAction("Users");
            }
        }

        [HttpPost]
        public async Task<ActionResult> EditUser(UserDetails userDetails)
        {
            ApplicationUser applicationUser = _applicationUserManager.FindByName(User.Identity.Name);



            if (!ModelState.IsValid)
            {
                var errors = string.Join(" | ",
                    ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                ModelState.AddModelError("", errors);

                ViewBag.ModelError = "Invalid Model State";

                return View(userDetails);
            }

            var newUserRoles = userDetails.AssignedRole.ToList();

            var persistedRoles = _applicationUserManager.GetRoles(userDetails.Id).ToList();


            foreach (var items in persistedRoles)
            {
                var roleRemoveResult = _applicationUserManager.RemoveFromRole(userDetails.Id, items);
                if (roleRemoveResult == null)
                {
                    TempData["Error"] = "Unable to remove user from role.";

                    return View(userDetails);
                }
            }
            foreach (var newRoles in newUserRoles)
            {
                var userRoleAddResult = _applicationUserManager.AddToRole(userDetails.Id, newRoles);
                if (userRoleAddResult == null)
                {
                    TempData["Error"] = "Unable to add user to role.";

                    return View(userDetails);
                }
            }

            var applicationUserDetails = _applicationUserManager.FindByName(User.Identity.Name);

            if (applicationUserDetails == null)
            {
                TempData["Error"] = "Unable to get logged in user details";

                return View(userDetails);
            }
            var userRole = _applicationUserManager.GetRoles(userDetails.Id).FirstOrDefault();

            var roleId = userRole != null ? _roleManager.FindByName(userRole) : null;

            ViewBag.RolesList = GetUserRoleInfo(string.Empty, roleId != null ? roleId.Id : string.Empty);

            var persisted = _applicationUserManager.FindById(userDetails.Id);

            DateTime? lockOutEndDate = null;

            if (userDetails.LockoutUser)
            {
                lockOutEndDate = DateTime.Now.AddYears(100);
            }

            persisted.FirstName = userDetails.FirstName;
            persisted.LastName = userDetails.LastName;
            persisted.LockoutEnabled = true;
            persisted.LockoutEndDateUtc = lockOutEndDate;
            persisted.Email = userDetails.Email;
            persisted.Id = userDetails.Id;
            persisted.PhoneNumber = userDetails.PhoneNumber;
            persisted.UserName = userDetails.UserName;


            var updateResult = await _applicationUserManager.UpdateAsync(persisted);

            if (updateResult.Succeeded)
            {
                TempData["Success"] = "User details updated successfully";

                return RedirectToAction("Users");
            }

            TempData["Error"] = "Unable to update user details.Please try again";

            return RedirectToAction("users");
        }

        [HttpGet]
        public ActionResult Users()
        {
            return View("users");
        }

        [HttpPost]
        public async Task<JsonResult> Users(JQueryDataTablesModel datatablemodel)
        {
            var allUsers = _applicationUserManager.Users.ToList();

            var userInfoList = new List<UserDetails>();

            if (allUsers != null && allUsers.Any())
            {

                foreach (var item in allUsers)
                {
                    userInfoList.Add(new UserDetails { UserName = item.UserName, Email = item.Email, FirstName = item.FirstName, LastName = item.LastName, PhoneNumber = item.PhoneNumber, LastPasswordChangeDate = item.LastPasswordChangeDate, CreatedDate = item.CreatedDate });
                }


            }

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var usersList = GetAllusers(startIndex: datatablemodel.iDisplayStart,
             pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
             totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, usersList: userInfoList);

            return Json(new JQueryDataTablesResponse<UserDetails>(items: usersList,
              totalRecords: totalRecordCount,
              totalDisplayRecords: searchRecordCount,
              sEcho: datatablemodel.sEcho));
        }

        [NonAction]
        public static IList<UserDetails> GetAllusers(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<UserDetails> usersList)
        {
            var users = usersList;
            totalRecordCount = users.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                users = users.Where(c => c.FirstName
                    .ToLower().Contains(searchString.ToLower()) || c.LastName
                    .ToLower().Contains(searchString.ToLower())
                    || c.PhoneNumber.ToLower().Contains(searchString.ToLower()) ||
                    c.Id.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = users.Count;

            IOrderedEnumerable<UserDetails> sortedTrans = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "FirstName":
                        sortedTrans = sortedTrans == null ? users.CustomSort(sortedColumn.Direction, tran => tran.FirstName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.FirstName);
                        break;
                    case "LastName":
                        sortedTrans = sortedTrans == null ? users.CustomSort(sortedColumn.Direction, tran => tran.LastName)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.LastName);
                        break;

                    case "PhoneNumber":
                        sortedTrans = sortedTrans == null ? users.CustomSort(sortedColumn.Direction, tran => tran.PhoneNumber)
                            : sortedTrans.CustomSort(sortedColumn.Direction, tran => tran.PhoneNumber);
                        break;

                }


            }

            return sortedTrans.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        private List<SelectListItem> GetUserRoleInfo(string userName, string selectedValue, bool filterRoles = true)
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = selectedValue == string.Empty, Text = "--Select Role--" });

            var roles = _roleManager.Roles.ToArray();

            Array.ForEach(roles,
                roleName =>
                {
                    rolesInfo.Add(new SelectListItem
                    {
                        Selected = roleName.Id == selectedValue,
                        Text = roleName.Name,
                        Value = roleName.Name
                    });
                });

            return rolesInfo;
        }
        public enum RecordStatus
        {
            [Description("Approved")]
            Approved,
            [Description("New")]
            New,
            [Description("Edited")]
            Edited,
            [Description("Rejected")]
            Rejected,
            [Description("Extracted")]
            Extracted,
            [Description("Locked")]
            Locked,
            [Description("Blacklisted")]
            Blacklisted,
            [Description("Processed")]
            Processed,
            [Description("Rehabilitated")]
            Rehabilitated,
            [Description("Key Account")]
            KeyAccount,
            [Description("Tricked")]
            Tricked
        }
        #endregion
    }
}