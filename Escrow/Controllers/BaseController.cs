﻿using Escrow.ApiRequests;
using Escrow.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Escrow.Controllers
{
    public class BaseController : Controller
    {
        protected static string _bearer;

        protected static HttpCookie Cookie { get; set; }


        public async Task CheckIfBearerTokenIsSet()
        {
            if (HttpContext.Session["Token"] == null)
            {

                string accessToken = await ClientHelper.GenerateToken();
                // Add another value.

                // Add it to the current web response.

                HttpContext.Session["Token"] = accessToken;
                _bearer = accessToken;

            }
            else
            {
                _bearer = HttpContext.Session["Token"].ToString();
            }



        }

        [NonAction]
        protected List<SelectListItem> DisputeCategorySelectListItems(string selectedValue)
        {
            List<SelectListItem> disputeCategoryItems = new List<SelectListItem>()
            {
                new SelectListItem {Selected = selectedValue == string.Empty, Text = "--Category--", Value = ""}
            };

            var items = Enum.GetValues(typeof(Dispute)).Cast<Dispute>().Select(v => new SelectListItem
            {
                Text = GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            disputeCategoryItems.AddRange(items);

            return disputeCategoryItems;
        }

        private static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

    }
}