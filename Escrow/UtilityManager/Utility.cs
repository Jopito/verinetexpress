﻿using System;
using System.Globalization;

namespace Escrow.UtilityManager
{
    public class Utility
    {
        public static DateTime ConvertToLocalDate(DateTime date)
        {

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time");

            DateTime actualTime = TimeZoneInfo.ConvertTimeFromUtc(date, timeZoneInfo);

            return actualTime;
        }

        public static DateTime FormalizeTranscationDate(string date, string time)
        {
            try
            {
                DateTime convertedDate = DateTime.ParseExact($"{date}", "d-M-yyyy", CultureInfo.InvariantCulture);


                convertedDate = ConvertToLocalDate(convertedDate);

                return convertedDate;
            }
            catch (Exception)
            {

                return new DateTime();
            }


        }
    }
}